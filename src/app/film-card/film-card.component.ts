import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-film-card',
  templateUrl: './film-card.component.html',
  styleUrls: ['./film-card.component.css']
})
export class FilmCardComponent implements OnInit {
  @Input()film;
  duree;
  realisateurs;
  acteurs;

  constructor() { }

  ngOnInit() {
    let duree;
    duree = this.film.duree.split(':');
    this.duree = duree[0] + 'h' + duree[1];

    let realisateurs = [];
    for(let realisateur of this.film.realisateurs) {
      realisateurs.push(realisateur.prenom + ' ' + realisateur.nom);
    }
    this.realisateurs = realisateurs.join(',');

    let acteurs = [];
    for(let acteur of this.film.acteurs) {
      acteurs.push(acteur.prenom + ' ' + acteur.nom);
    }
    this.acteurs = acteurs.join(', ');
  }

}
