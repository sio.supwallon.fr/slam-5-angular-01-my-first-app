import { Component, OnInit } from '@angular/core';
import { films } from '../films';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit {
  films = films;

  constructor() { }

  ngOnInit() {
  }

}
