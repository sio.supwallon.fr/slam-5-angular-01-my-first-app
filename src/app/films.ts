export const films = [
    {
        titre: 'D.A.R.Y.L.',
        duree: '1:39:00',
        realisateurs: [{prenom: 'Simon', nom: 'Wincer'}],
        acteurs: [
            {prenom: 'Barret', nom: 'Oliver'},
            {prenom: 'Mary', nom: 'Beth Hurt'},
            {prenom: 'Michael', nom: 'McKean'}
        ],
        image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg'
    },
    {
        titre: 'BOHEMIAN RHAPSODY',
        duree: '2:15:00',
        realisateurs: [{prenom: 'Bryan', nom: 'Singer'}],
        acteurs: [
            {prenom: 'Rami', nom: 'Malek'},
            {prenom: 'Gwilym', nom: 'Lee'},
            {prenom: 'Lucy', nom: 'Boynton'}
        ],
        image: '2028013.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg'
    },
    {
        titre: 'LA CITÉ DE LA PEUR',
        duree: '1:40:00',
        realisateurs: [{prenom: 'Alain', nom: 'Berbérian'}], 
        acteurs: [
            {prenom: 'Chantal', nom: 'Lauby'},
            {prenom: 'Alain', nom: 'Chabat'},
            {prenom: 'Dominique', nom: 'Farrugia'}
        ],
        image: '1132544.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg'
    },
    {
        titre: 'USUAL SUSPECTS',
        duree: '1:46:00',
        realisateurs: [{prenom: 'Bryan', nom: 'Singer'}], 
        acteurs: [
            {prenom: 'Chazz', nom: 'Palminteri'},
            {prenom: 'Kevin', nom: 'Spacey'},
            {prenom: 'Gabriel', nom: 'Byrne'}
        ],
        image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg'
    },
];